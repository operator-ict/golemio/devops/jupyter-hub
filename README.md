# Jupyter for Golemio - Implementation Guide

(https://jupyter.golemio.cz/)

based on Zero To [JupyterHub](https://zero-to-jupyterhub.readthedocs.io/en/stable/)
An App has two basic layers:

1. `JupyterHub`:

Server for authentication, security, networking and spawing of individual users' Jupyter environment

2. `Singleuser` individual jupyter environment:

Individual environment for each user

## features:

* GitLab OAuth

* Private volumes for each user and shared volume for all users

* Globally predefined set of packages 

* Set of ENV variables available for users (links to  DBs, S3 etc.)

* JupyterLab Git Extension

## User environment
### Python packages in default env
* in [`./singleuser/requirements.txt`](./singleuser/requirements.txt)

### JupyterLab extensions
*  https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/singleuser/Dockerfile#L41

### Whitelist users
*https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/k8s/config.yaml#L12

### Environment variables
* https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/k8s/config.yaml#L45) and [here](https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/k8s/config.yaml#L55

### Private storage space
* https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/k8s/config.yaml#L133

### Shared storage space
* https://gitlab.com/operator-ict/golemio/devops/jupyter-hub/-/blob/master/k8s/jupyterhub-shared-volume.yaml#L12


## Configuration
via [Helm config](./k8s/config.yaml):

**Hub:**

* Users whitelists
* Database connection to keep state
* Gitlab Oauth

**Singleuser:**

* Singleuser environment variables
* singleuser dedicated resources (cpu and memory)
* Lifecycle hooks
* Volumes

## Single-user Notebook Image [`./singleuser/Dockerfile`](./singleuser/Dockerfile)

Based on an image defined in `DOCKER_NOTEBOOK_IMAGE` env variable

* Install Python packages for users (see [`./singleuser/requirements.txt`](./singleuser/requirements.txt))

## .gitlab-ci.yml
* Build singleuser image
* define DOCKER_NOTEBOOK_IMAGE
* deploy to Kubernetes


## Kubernetes helm

https://github.com/jupyterhub/zero-to-jupyterhub-k8s
https://github.com/jupyterhub/helm-chart

### Install

`kubectl create namespace jupyterhub`

`kubectl config set-context --current --namespace=jupyterhub`

### IngressRoute

`kubectl apply -f jupyter-ingressroute.yaml`

### secrets

umistene v repository [golemio-rabin](https://gitlab.com/operator-ict/security/golemio-rabin)

`kubectl apply -f jupyterhub/jupyter-secret.yaml`
#### helm jupyterhub

```
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
```

Konfigurace Jupyter Hub pro kubernetes je uložena v souboru `k8s/config.yaml`

```
helm upgrade \
  --cleanup-on-fail \
  --install jupyterhub jupyterhub/jupyterhub \
  --namespace jupyterhub \
  --create-namespace \
  --values k8s/config.yaml
```

You can find if the hub and proxy is ready by doing:

`kubectl --namespace=jupyterhub get pod`

and watching for both those pods to be in status 'Running'.

You can find the public IP of the JupyterHub by doing:

`kubectl --namespace=jupyterhub get svc proxy-public`
